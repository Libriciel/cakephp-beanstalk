<?php
/**
 * Entité de la table beanstalk_workers
 *
 * Défini des champs virtuels
 *
 * @category    Beanstalk
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */

namespace Beanstalk\Model\Entity;

use Cake\ORM\Entity;

/**
 * Classe de l'entité
 *
 * Permet notamment de savoir sous unix si le worker de l'entité est en cours de
 * fonctionnement.
 */
class BeanstalkWorker extends Entity
{
    /**
     * Champs virtuels
     *
     * @var array
     */
    protected $_virtual = ['running'];

    /**
     * Champ virtuel
     *
     * @return bool
     */
    protected function _getRunning()
    {
        // Cas docker, on part du principe que si on obtient une ip, c'est que le container tourne
        if (!empty($this->_fields['hostname']) && $this->_fields['hostname'] !== gethostname()) {
            $ip = gethostbyname($this->_fields['hostname']);
            return $ip !== $this->_fields['hostname'];
        }
        if (!empty($this->_fields['pid'])) {
            // $ps ~= " 1782 ?        00:00:00 /var/www/html/bin/cake worker exemple"
            $ps = exec(sprintf('ps -p %d', (int)$this->_fields['pid']));
            return preg_match('/^\s*(\d+)/', $ps, $m)
                && (int)$m[1] === $this->_fields['pid'];
        }
        return false;
    }
}
