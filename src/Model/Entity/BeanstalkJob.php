<?php
/**
 * Entité de la table beanstalk_jobs
 *
 * Défini des champs virtuels afin d'exploiter les informations détenues par le
 * serveur Beanstalk.
 *
 * @category    Beanstalk
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */

namespace Beanstalk\Model\Entity;

use Beanstalk\Model\Table\BeanstalkJobsTable;
use Cake\ORM\Entity;
use Exception;

/**
 * Classe de l'entité
 *
 * <b>beanstalkStats()</b> permet de récupérer et de stocker les informations
 * beanstalk sur le job de l'entité.
 * Cette information est récupérée par les méthodes des champs virtuels.
 *
 * <b>getServerBeanstalkStatus()</b> permet de récupérer les informations du
 * serveur beanstalk du job.
 */
class BeanstalkJob extends Entity
{
    /**
     * @var array Champs virtuels
     */
    protected $_virtual = [
        'age',
        'job_statetrad',
        'object_data',
        'related_id',
        'state',
        'statetrad',
    ];

    /**
     * @var array Stock le résultat de la function beanstalkStats
     */
    protected $_stats;

    /**
     * Donne une instance de Beanstalk
     * @return null
     * @deprecated since v4
     */
    public function getBeanstalk()
    {
        return null;
    }

    /**
     * Récupère les informations beanstalk sur un job en fonction de son id
     * @return array
     * @throws Exception
     * @deprecated
     */
    public function beanstalkStats()
    {
        return [];
    }

    /**
     * Demande le statut du serveur Beanstalkd
     * @return array
     * @throws Exception
     * @deprecated
     */
    public function getServerBeanstalkStatus()
    {
        return [
            'current-jobs-urgent' => null,
            'current-jobs-ready' => null,
            'current-jobs-reserved' => null,
            'current-jobs-delayed' => null,
            'current-jobs-buried' => null,
            'current-workers' => null,
            'uptime' => null,
        ];
    }

    /**
     * Returns the fields that will be serialized as JSON
     *
     * @return array
     */
    public function jsonSerialize(): array
    {
        $data = parent::jsonSerialize();
        $data['data'] = $data['data'] ?? null;
        $data['data'] = is_resource($data['data'])
            ? stream_get_contents($data['data'])
            : $data['data'];
        return $data;
    }

    /**
     * Converti les ressources en base64
     * @return array
     */
    public function toArray(): array
    {
        $data = parent::toArray();
        $data['data'] = $data['data'] ?? null;
        $data['data'] = is_resource($data['data'])
            ? stream_get_contents($data['data'])
            : $data['data'];
        return $data;
    }

    /**
     * Champ virtuel
     * @return mixed
     * @throws Exception
     */
    protected function _getObjectData()
    {
        $data = $this->_fields['data'] ?? '';
        if (is_resource($data)) {
            rewind($data);
            $data = stream_get_contents($data);
        }
        return json_decode($data, true);
    }

    /**
     * Défini data
     * @param array $data
     * @return false|string
     */
    protected function _setData($data)
    {
        return json_encode($data, JSON_UNESCAPED_SLASHES);
    }

    /**
     * Champ virtuel
     * @return string
     * @throws Exception
     */
    protected function _getRelatedId()
    {
        $data = $this->_getObjectData();
        if ($data && !empty($data['id'])) {
            return $data['id'];
        }
        return null;
    }

    /**
     * Ancien champ virtuel d'état
     * @return string
     */
    protected function _getStatetrad(): ?string
    {
        return $this->_getJobStatetrad();
    }

    /**
     * Traduction du champ job_state
     * @return string
     */
    protected function _getJobStatetrad(): string
    {
        $state = '';
        switch ($this->_fields['job_state'] ?? null) {
            case BeanstalkJobsTable::S_FAILED:
                $state = __dx('beanstalk_job', 'job_state', "échec");
                break;
            case BeanstalkJobsTable::S_DELAYED:
                $state = __dx('beanstalk_job', 'job_state', "programmé");
                break;
            case BeanstalkJobsTable::S_PAUSED:
                $state = __dx('beanstalk_job', 'job_state', "en pause");
                break;
            case BeanstalkJobsTable::S_PENDING:
                $state = __dx('beanstalk_job', 'job_state', "en attente");
                break;
            case BeanstalkJobsTable::S_WORKING:
                $state = __dx('beanstalk_job', 'job_state', "en cours");
                break;
        }
        return $state;
    }
}
