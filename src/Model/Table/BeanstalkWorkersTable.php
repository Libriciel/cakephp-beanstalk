<?php
/**
 * Table beanstalk_workers
 *
 * Défini la validation
 *
 * @category    Beanstalk
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */

namespace Beanstalk\Model\Table;

use ArrayObject;
use Beanstalk\Model\Entity\BeanstalkWorker;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Classe de la table
 */
class BeanstalkWorkersTable extends Table
{
    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->hasMany('BeanstalkJobs');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * La table est t-elle synchronisée ?
     *
     * @var boolean
     */
    public $sync = false;

    /**
     * A lancer avant un find
     *
     * @param Event $Event
     */
    public function beforeFind(/** @noinspection PhpUnusedParameterInspection */Event $Event)
    {
        if (!$this->sync) {
            $this->sync = true;
            $this->sync();
        }
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @noinspection PhpUnusedParameterInspection
     */
    public function beforeDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        $this->BeanstalkJobs->updateAll(
            ['beanstalk_worker_id' => null],
            ['beanstalk_worker_id' => $entity->id]
        );
    }

    /**
     * Synchronise la base de donnée avec Beanstalk
     *
     * @return boolean
     */
    public function sync()
    {
        $query = $this->find()->select(['id', 'hostname', 'pid']);
        /** @var BeanstalkWorker $entity */
        foreach ($query as $entity) {
            if (!$entity->get('running')) {
                $this->delete($entity);
            }
        }

        return true;
    }
}
