<?php
/**
 * Beanstalk\Command\JobResumeCommand
 */

namespace Beanstalk\Command;

use Beanstalk\Model\Table\BeanstalkJobsTable;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Exception;

/**
 * Permet de relancer un job
 * ex: bin/cake job resume 4531
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class JobResumeCommand extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'job resume';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->addArgument(
            'job_id',
            [
                'help' => __("Identifiant beanstalk du job"),
                'required' => true,
            ]
        );
        return $parser;
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        /** @var BeanstalkJobsTable $Jobs */
        $Jobs = $this->fetchTable('BeanstalkJobs');
        $job = $Jobs->find()
            ->where(['BeanstalkJobs.id' => $args->getArgument('job_id')])
            ->first();
        if (!$job) {
            $io->abort(
                __(
                    "Le job (job_id={0}) n'a pas été trouvé en base de données",
                    $args->getArgument('job_id')
                )
            );
        }
        $Jobs->transition($job, BeanstalkJobsTable::T_RESUME);
        $Jobs->saveOrFail($job);
        $io->out('done');
    }
}
