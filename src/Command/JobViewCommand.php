<?php
/**
 * Beanstalk\Command\JobViewCommand
 */

namespace Beanstalk\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Utility\Hash;
use Exception;

/**
 * Permet de visualiser un job
 * ex: bin/cake job view 4531
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class JobViewCommand extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'job view';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->addArgument(
            'job_id',
            [
                'help' => __("Identifiant beanstalkd du job"),
                'required' => true,
            ]
        );
        return $parser;
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $Jobs = $this->fetchTable('BeanstalkJobs');
        $job = $Jobs->find()
            ->where(['BeanstalkJobs.id' => $args->getArgument('job_id')])
            ->contain(['Users'])
            ->first();
        if (!$job) {
            $io->abort(
                __(
                    "Le job (job_id={0}) n'a pas été trouvé en base de données",
                    $args->getArgument('job_id')
                )
            );
        }
        $created = $job->get('created');
        $user = Hash::get($job, 'user.username');
        if ($name = Hash::get($job, 'user.name')) {
            $user .= " ($name)";
        }
        $data = $job->get('data') ?: [];
        if (is_resource($data)) {
            $data = json_decode(stream_get_contents($data));
        }
        $arr = [
            'id' => $job->get('id'),
            'tube' => $job->get('tube'),
            'priority' => $job->get('priority'),
            'created' => $created,
            'user' => $user,
            'delay' => $job->get('delay'),
            'ttr' => $job->get('ttr'),
            'errors' => $job->get('errors'),
            'job_state' => $job->get('job_state'),
            'last_state_update' => $job->get('last_state_update'),
            'data' => http_build_query((array)$data, '', ', '),
        ];
        $data = [];
        foreach ($arr as $key => $value) {
            $data[] = [$key, (string)$value];
        }
        $io->helper('Table')->output($data);
    }
}
