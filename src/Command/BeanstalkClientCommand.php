<?php
/**
 * Beanstalk\Command\JobListCommand
 */

namespace Beanstalk\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Core\Configure;
use Exception;
use React\Socket\ConnectionInterface;
use React\Socket\TcpConnector;

/**
 * Permet de liste les jobs
 * ex: bin/cake job list test --page 1
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class BeanstalkClientCommand extends Command
{
    /**
     * Start the Command and interactive console.
     *
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @return void The exit code or null for success
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $config = Configure::read('Beanstalk') + [
            'server_host' => '127.0.0.1',
            'server_port' => 11300,
        ];
        // converti un dns en ip (nécéssaire pour TcpConnector)
        if (@inet_pton($config['host']) === false) {
            $config['server_host'] = gethostbyname($config['server_host']);
        }
        $uri = sprintf('tcp://%s:%d', $config['server_host'], $config['server_port']);
        $client = new TcpConnector();
        $client
            ->connect($uri)
            ->then(
                function (ConnectionInterface $connection) {
                    $connection->write("test\r\n");
                    $connection->end();
                }
            )
            ->otherwise(
                function ($e) use ($io) {
                    if ($e instanceof Exception) {
                        $io->err($e->getMessage());
                        throw $e;
                    }
                }
            );
    }
}
