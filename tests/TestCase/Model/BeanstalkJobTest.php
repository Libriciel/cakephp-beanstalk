<?php

namespace Beanstalk\Test\TestCase\Model;

use Beanstalk\Model\Entity\BeanstalkJob;
use Beanstalk\Model\Table\BeanstalkJobsTable;
use Beanstalk\Test\Mock\MockedBeanstalk;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Pheanstalk\Connection;
use Pheanstalk\Pheanstalk;

/**
 * Class BeanstalkJobTest
 * @package Beanstalk\Test\Model
 * @property BeanstalkJob $Entity
 */
class BeanstalkJobTest extends TestCase
{
    public $fixtures = [
        'app.BeanstalkJobs',
    ];

    const TUBE = 'testunit-beanstalkjobs';

    /**
     * @var BeanstalkJobsTable
     */
    private $BeanstalkJobs;

    public function setUp(): void
    {
        parent::setUp();
        Configure::write('Beanstalk.classname', MockedBeanstalk::class);

        $Connection = $this->createMock(Connection::class);
        $Connection->method('isServiceListening')->willReturn(true);

        $this->BeanstalkJobs = TableRegistry::getTableLocator()->get('Beanstalk.BeanstalkJobs');
    }

    public function tearDown(): void
    {
        parent::tearDown();
        $this->BeanstalkJobs->deleteAll(['tube' => self::TUBE]);
        TableRegistry::getTableLocator()->clear();
    }

    protected function mockPheanstalk($Job = null)
    {
        $Connection = $this->createMock(Connection::class);
        $Connection->method('isServiceListening')->willReturn(true);

        $Pheanstalk = $this->createMock(Pheanstalk::class);
        $Pheanstalk->method('useTube')->willReturn($Pheanstalk);
        $Pheanstalk->method('watchOnly')->willReturn($Pheanstalk);
        $Pheanstalk->method('putInTube')->willReturn(1);
        $Pheanstalk->method('delete')->willReturn($Pheanstalk);
        $Pheanstalk->method('bury')->willReturn($Pheanstalk);
        $Pheanstalk->method('release')->willReturn($Pheanstalk);
        $Pheanstalk->method('touch')->willReturn($Pheanstalk);
        $Pheanstalk->method('watch')->willReturn($Pheanstalk);
        if ($Job) {
            $Pheanstalk->method('reserve')->willReturn($Job);
            $Pheanstalk->method('peek')->willReturn($Job);
        }
        $Pheanstalk->method('statsJob')->willReturn(
            [
                "id" => "1",
                "tube" => self::TUBE,
                "state" => "reserved",
                "pri" => "1024",
                "age" => "2229",
                "delay" => "0",
                "ttr" => "60",
                "time-left" => "51",
                "file" => "540",
                "reserves" => "2",
                "timeouts" => "0",
                "releases" => "1",
                "buries" => "0",
                "kicks" => "0",
            ]
        );
        $Pheanstalk->method('statsTube')->willReturn(
            (object)[
                "name" => self::TUBE,
                "current-jobs-urgent" => "0",
                "current-jobs-ready" => "1",
                "current-jobs-reserved" => "0",
                "current-jobs-delayed" => "0",
                "current-jobs-buried" => "0",
                "total-jobs" => "2",
                "current-using" => "1",
                "current-watching" => "1",
                "current-waiting" => "0",
                "cmd-delete" => "0",
                "cmd-pause-tube" => "0",
                "pause" => "0",
                "pause-time-left" => "0",
            ]
        );
        $Pheanstalk->method('stats')->willReturn(
            [
                'current-jobs-urgent' => "0",
                'current-jobs-ready' => "1",
                'current-jobs-reserved' => "0",
                'current-jobs-delayed' => "0",
                'current-jobs-buried' => "0",
                'current-workers' => "0",
                'uptime' => "0",
            ]
        );
        $Pheanstalk->method('getConnection')->willReturn($Connection);

        MockedBeanstalk::$StaticPheanstalk = $Pheanstalk;
        return $Pheanstalk;
    }

    public function testOptions()
    {
        $options = $this->BeanstalkJobs->options('state');
        $this->assertNotEmpty($options['ready']);

        $options = $this->BeanstalkJobs->options('notexists');
        $this->assertEquals([], $options);
    }
}
