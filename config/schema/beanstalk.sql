BEGIN;

CREATE TABLE beanstalk_jobs (
    id SERIAL NOT NULL PRIMARY KEY,
    jobid INTEGER NOT NULL,
    tube VARCHAR(255) DEFAULT 'default',
    priority INTEGER DEFAULT 1024,
    delay INTEGER DEFAULT 0,
    ttr INTEGER DEFAULT 60,
    last_status VARCHAR(255) DEFAULT 'ready',
    errors TEXT DEFAULT null,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    beanstalk_worker_id INTEGER,
    object_foreign_key INTEGER,
    object_model VARCHAR(255),
    job_state VARCHAR(255),
    last_state_update TIMESTAMP,
    states_history TEXT
);

CREATE TABLE beanstalk_workers (
    id          SERIAL NOT NULL PRIMARY KEY,
    name        VARCHAR(255),
    tube        VARCHAR(255),
    path        VARCHAR(255),
    pid         INTEGER,
    last_launch TIMESTAMP,
    hostname    VARCHAR(255)
);

COMMIT;
