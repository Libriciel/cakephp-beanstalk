<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class RemoveJobidFromBeanstalkJobs extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('beanstalk_jobs');
        $table->removeColumn('jobid');
        $table->removeColumn('last_status');
        $table->update();
    }
    public function down()
    {
        $table = $this->table('beanstalk_jobs');
        $table->addColumn('jobid', 'integer', [
            'default' => null,
            'limit' => 10,
            'null' => false,
        ]);
        $table->addColumn('last_status', 'string', [
            'default' => 'ready',
            'limit' => 255,
            'null' => true,
        ]);
        $table->update();
    }
}
