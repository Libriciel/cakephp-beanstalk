<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AddStateToBeanstalkJobs extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('beanstalk_jobs');
        $table->addColumn('beanstalk_worker_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ]);
        $table->addColumn('object_model', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('object_foreign_key', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ]);
        $table->addColumn('job_state', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('last_state_update', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('states_history', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addForeignKey(
            'beanstalk_worker_id',
            'beanstalk_workers',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'SET NULL'
            ]
        );
        $table->update();
    }

    public function down()
    {
        $table = $this->table('beanstalk_jobs');
        $table->removeColumn('worker_id');
        $table->removeColumn('object_model');
        $table->removeColumn('object_foreign_key');
        $table->removeColumn('job_state');
        $table->removeColumn('last_state_update');
        $table->removeColumn('states_history');
        $table->save();
    }
}
