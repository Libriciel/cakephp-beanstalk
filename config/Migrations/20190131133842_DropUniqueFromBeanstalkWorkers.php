<?php
use Migrations\AbstractMigration;

class DropUniqueFromBeanstalkWorkers extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('beanstalk_workers');
        $table->removeIndex(['name']);
        $table->save();
    }

    public function down()
    {
        $table = $this->table('beanstalk_workers');
        $table->addIndex(['name'], ['unique' => true]);
        $table->save();
    }
}
